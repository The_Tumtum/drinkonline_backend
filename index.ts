var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);

var amountPlayersOnline = 0;

http.listen(4001, () => {
  console.log('listening on *:4001');
});

io.on('connection', (socket) => {
	amountPlayersOnline++;
	socket.emit('connected');
	io.emit('amountPlayersOnline', amountPlayersOnline);
  
	socket.on('disconnecting', () => {
		Object.keys(socket.rooms).forEach(function(roomName){
			if(roomName.length === 6){
				var room = io.sockets.adapter.rooms[roomName];
				if(room.length !== 1){
					let players = Object.keys(room.sockets);
					if(players[0] == socket.id){
						socket.to(roomName).emit('hostLeave',  io.sockets.connected[players[1]].nickname);
					}else{
						socket.to(roomName).emit('playerLeave', socket.nickname);
					}
				}
			}
		});
	});
  
	socket.on('disconnect', () => {
		amountPlayersOnline--;
		io.emit('amountPlayersOnline', amountPlayersOnline);
	});
	
	socket.on('setNickname', function(nickname){
		socket.nickname = nickname;
	});
	
	socket.on('createRoom', () => {
		let noRoomJoined = true;
		let attempts = 0;
		let roomCode;
		do{
			roomCode = '';
			for (let i = 0; i<6; i++) {
			  // we get a random number between 0 and 25 so its within the length of the alphabet
			  // We add 65 to get the character number of the uppercase letter of the random number. EX: random number is 3 so we get letter C.
			  roomCode = roomCode.concat(String.fromCharCode(Math.floor(Math.random() * 26) + 65));
			}

			// Check if the room is not already taken OR if the room youre trying to take has been idle for more than 20 minutes
			if(io.sockets.adapter.rooms[roomCode] == null || new Date(io.sockets.adapter.rooms[roomCode].lastActivityTime.getTime() + (20*60*1000)) <= new Date()){
				io.to(roomCode).emit('roomInactive');
				socket.join(roomCode);
				socket.emit('roomID', roomCode);
				io.sockets.adapter.rooms[roomCode].openToJoin = true;
				io.sockets.adapter.rooms[roomCode].lastActivityTime = new Date();
				noRoomJoined = false;
			}
			attempts++;
		}while(noRoomJoined && attempts<50);
		
		if(noRoomJoined){
			socket.emit('noRooms');
		}
    });
	
	socket.on('openCloseRoom', (roomName) => {
		// Make sure the request was sent by the lobby host.
		if(socket.id === Object.keys(io.sockets.adapter.rooms[roomName].sockets)[0]){
			io.sockets.adapter.rooms[roomName].openToJoin = !io.sockets.adapter.rooms[roomName].openToJoin;
			io.to(roomName).emit('openCloseRoom', io.sockets.adapter.rooms[roomName].openToJoin);
		}
	});
	
	socket.on('joinRoom', function(roomName, callback){
		let thisRoom = io.sockets.adapter.rooms[roomName];
		if(thisRoom && thisRoom.openToJoin){
			const players = Object.keys(thisRoom.sockets);
			let nameCount = 1;
			let originalName = socket.nickname;
			//Add a number behind the name if the name already exists in the room.
			for(let i=0; i<players.length; i++){
				if(io.sockets.connected[players[i]].nickname == socket.nickname){
					socket.nickname = originalName.concat(nameCount); 
					nameCount++;
					i = 0;
				}
			}
			
			socket.join(roomName);
			socket.to(roomName).emit('playerJoin', socket.nickname);
			socket.to(players[0]).emit('playerWantsPlayerData', socket.id);

			callback('success', socket.nickname);
		}else{
			callback('fail');
		}
		// ----------------------------------------------------------------------- Check if nickname is not the same as another nickname and add number to the socket nickname
    });
	
	socket.on('givePlayerDataToPlayer', (id, data) => {
		socket.to(id).emit('receivePlayerData', data);
    });
	
	socket.on('kickPlayer', (room, name, i) => {
		// Make sure the request was sent by the lobby host.
		if(socket.id === Object.keys(io.sockets.adapter.rooms[room].sockets)[0]){
			let myRoom = io.sockets.adapter.rooms[room];
			let kickPlayerid = Object.keys(myRoom.sockets)[i];
			io.to(room).emit('playerLeave', name);
			socket.to(kickPlayerid).emit('kicked');
			io.sockets.connected[kickPlayerid].leave(room);
		}
    });
	
	socket.on('leaveRoom', (roomName) => {
		let room = io.sockets.adapter.rooms[roomName];
		socket.leave(roomName);
		if(room.length !== 0){
			let players = Object.keys(room.sockets);
			if(players[0] == socket.id){
				socket.to(roomName).emit('hostLeave', io.sockets.connected[players[0]].nickname);
			}else{
				socket.to(roomName).emit('playerLeave', socket.nickname);
			}
		}
    });
	
	socket.on('nextQuestion', (roomName, gameType, nextGameNumber, newGameData, personReadingQuestionIndex) => {
		let room = io.sockets.adapter.rooms[roomName];
		let players = Object.keys(room.sockets);
		
		// Make sure the request was sent by the lobby host.
		if(socket.id === players[0]){
			io.to(roomName).emit("getNewGameData", gameType, nextGameNumber, newGameData, personReadingQuestionIndex);
		}
    });
});